import tweepy
import config

from typing import List

class Tweets:

    @staticmethod
    def _get_images_urls(media_keys: List[str], includes_media: List[tweepy.Media]):
        urls = [media.url for media in includes_media if media.media_key in media_keys]

        return urls

    @staticmethod
    def _get_author(author_id: str, includes_users: List[tweepy.User]):
        author = [
            {"name": user.name, "username": user.username}
            for user in includes_users
            if user.id == author_id
        ][0]

        return author

    @staticmethod
    def search(query):

        client = tweepy.Client(
            consumer_key=config.consumer_key,
            consumer_secret=config.consumer_secret,
            bearer_token=config.bearer_token,
        )

        search_results = client.search_recent_tweets(
            query=f"{query} has:media -is:retweet",
            media_fields=["media_key", "url", "preview_image_url"],
            expansions=["attachments.media_keys", "author_id"],
            tweet_fields=["author_id"],
        )

        tweets = []

        for tweet in search_results.data:
            author = Tweets._get_author(tweet.author_id, search_results.includes["users"])
            tweets.append(
                {
                    "id": tweet.id,
                    "text": tweet.text,
                    "image_urls": Tweets._get_images_urls(
                        tweet.data["attachments"]["media_keys"],
                        search_results.includes["media"],
                    ),
                    "author_id": tweet.author_id,
                    "author_name": author["name"],
                    "author_username": author["username"],
                }
            )


        return tweets